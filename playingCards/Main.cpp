// Enum and Struct Demo
// Benjamin Hohn

#include <iostream>
#include <conio.h>

enum Suit {

	HEART, CLUB, SPADE, DIAMOND
};

enum Rank {

	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

struct Card {

	Suit suit;
	Rank rank;
}; 
Card HighCard(Card card1, Card card2);
void PrintCard(Card card);

int main() {

	Card card1;
	card1.suit = HEART;
	card1.rank = FOUR;

	Card card2;
	card2.suit = HEART;
	card2.rank = FOUR;

	PrintCard(HighCard(card1, card2));

	_getch();
	return 0;
}

void PrintCard(Card card) {
	switch (card.rank) {
	case 2:
		std::cout << "2 of ";
		break;
	case 3:
		std::cout << "3 of ";
		break;
	case 4:
		std::cout << "4 of ";
		break;
	case 5:
		std::cout << "5 of ";
		break;
	case 6:
		std::cout << "6 of ";
		break;
	case 7:
		std::cout << "7 of ";
		break;
	case 8:
		std::cout << "8 of ";
		break;
	case 9:
		std::cout << "9 of ";
		break;
	case 10:
		std::cout << "10 of ";
		break;
	case 11:
		std::cout << "Jack of ";
		break;
	case 12:
		std::cout << "Queen of ";
		break;
	case 13:
		std::cout << "King of ";
		break;
	case 14:
		std::cout << "Ace of ";
		break;
	}

	switch (card.suit) {
	case 0:
		std::cout << "Hearts";
		break;
	case 1:
		std::cout << "Clubs";
		break;
	case 2:
		std::cout << "Spades";
		break;
	case 3:
		std::cout << "Diamonds";
		break;

	}
}
	Card HighCard(Card card1, Card card2) {
		if (card1.rank > card2.rank) return card1;
		else return card2;
	}

